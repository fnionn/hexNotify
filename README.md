# A bot to notify the users of the HexDSL Discord whenever he goes live on his OwnCast Instance

## Setup

- Copy config.json.example to config.json and fill in the respective values.
- Start by running: `npm start`
