const fetch = require("node-fetch");
const {
  CHECK_INTERVAL,
  DISCORD_BOT_TOKEN,
  CHANNEL_ID,
} = require("./config.json");
const Discord = require("discord.js");
const client = new Discord.Client();
const Parser = require("rss-parser");
const parser = new Parser();
const Enmap = require("enmap");
const db = new Enmap({ name: "BotDatabase" });

client.on("ready", () => {
  console.info("Online!")
  if (!client.channels.cache.get(CHANNEL_ID)) {
    console.log("A channel with that ID does not exist!");
    process.exit(0);
  }
  !db.has("streamNotified") ? db.set("streamNotified", false) : true;
  !db.has("currentRSSPost") ? db.set("currentRSSPost", false) : true;
});

client.on("message", async (message) => {
  if (message.content.includes("ping") && message.channel.type === "dm") {
    client.users.cache
      .get(message.author.id)
      .send(`I exist. ${client.ws.ping}ms!`);
  }
});

client.on("guildMemberAdd", (member) => {
  const ch = client.channels.cache.get(CHANNEL_ID);
  ch.send(`New User Has Joined! Welcome <@${member.id}>`);
});

setInterval(async () => {
  // Check if Hex is live
  const res = await fetch("https://tv.hexdsl.co.uk/api/status");
  const response = await res.json();
  const live = response.online;
  if (live == true) {
    const hasNotified = db.get("hasNotified");
    if (response.online == true && !hasNotified) {
      const ch = client.channels.cache.get(CHANNEL_ID);
      ch.send(
        new Discord.MessageEmbed({
          title: "=> https://tv.hexdsl.co.uk/",
          color: "RANDOM",
          description: "HexDSL is live!",
          url: "https://tv.hexdsl.co.uk/",
        })
      );
      db.set("hasNotified", true);
    }
  }

  live == false ? db.set("hasNotified", false) : true;

  // RSS Check
  const feed = await parser.parseURL("https://hexdsl.co.uk/rss.xml");
  const currentRSSPost = db.get("currentRSSPost");
  const latestPost = feed.items[0];
  if (latestPost.title !== currentRSSPost) {
    client.channels.cache.get(CHANNEL_ID).send(
      new Discord.MessageEmbed({
        title: `=> ${latestPost.title}`,
        url: latestPost.link,
        color: "RANDOM",
      })
    );
    db.set("currentRSSPost", latestPost.title);
  }
}, CHECK_INTERVAL);

client.login(DISCORD_BOT_TOKEN);
